defmodule Pigate.MQTT.Handler do
  use Tortoise.Handler

  def handle_message([topic], payload, state) do
    # unhandled message! You will crash if you subscribe to something
    # and you don't have a 'catch all' matcher; crashing on unexpected
    # messages could be a strategy though.

    Pigate.Mqtt.Store.push_message(topic, payload)
    {:ok, state}
  end
end
