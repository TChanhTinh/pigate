defmodule Pigate.Mqtt.Store do
  @moduledoc """
  This store and dispatch MQTT message
  """

  use GenServer

  def start_link(_opt) do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  def push_message(topic, payload) do
    GenServer.cast(__MODULE__, {:push, topic, payload, DateTime.utc_now()})
    broadcast({:ok, topic, payload}, :message_pushed)
  end

  def get_message(topic) do
    GenServer.call(__MODULE__, {:get, topic})
  end

  def get_messages() do
    list = GenServer.call(__MODULE__, :get)
  end

  def init(_init_arg) do
    keyword = Keyword.new()
    {:ok, keyword}
  end

  def handle_cast({:push, topic, payload, timestamp}, state) do
    keyword = state
    |> Keyword.put(String.to_atom(topic), payload <> " [#{DateTime.to_iso8601(timestamp)}]")

    {:noreply,
    keyword}
    #device_state ++ state}
  end

  def handle_call(:get, _from, state) do
    {:reply, state, state}
  end

  def handle_call({:get, topic}, _from, state) do
    device_state = state
    |> Keyword.get(String.to_atom(topic))

    {:reply, device_state, state}
  end

  def subscribe do
    Phoenix.PubSub.subscribe(Pigate.PubSub, "messages")
  end

  def broadcast({:error, _reason} = error, _event), do: error
  def broadcast({:ok, topic, payload} = message, event) do
    Phoenix.PubSub.broadcast(Pigate.PubSub, "messages", {event, message})
    {:ok, message}
  end
end
