const mqtt = require('mqtt');
// var socketClient = require('socket.io-client')('https://calm-brook-07121.herokuapp.com/');
var Topic = '#'; //subscribe to all topics
const {atob} = require('atob');
var converter = require('hex2dec');
const { idMQTT, ipMQTT } = require('./keys');
class MqttHandler {
  constructor() {
    this.mqttClient = null;
    this.host = ipMQTT;
    this.username = ''; // mqtt credentials if these are needed to connect
    this.password = '';
  }
  connect() {
    this.mqttClient = mqtt.connect(this.host, { clientId: "13c30a4de1cd45c396814208916d8369", qos: 1 });
    this.mqttClient.on('error', (err) => {
      console.log(err);
      this.mqttClient.end();
    });

    this.mqttClient.on('connect', () => {
      console.log(`mqtt client connected`);
    });
    this.mqttClient.subscribe(Topic, { qos: 0 });
    this.mqttClient.on('message', function (topic, message) {
      var search = topic.toString().indexOf('location');
      var searchconfig = topic.toString().indexOf('config');
      var searchStatus = topic.toString().indexOf('status');
      var searchDetect = topic.toString().indexOf('data');
      var searchGateway = topic.toString().indexOf('gateway');
      if(searchGateway != -1)
      {
        var label = 'GATEWAY';
      }

      // Tag position message
      if(search != -1)
      {
        let tem = null;
        tem = message.toString('utf8');
        if(tem.length > 0)
        {
          let me = JSON.parse(tem)

          if ((me.position.x) != "NaN")
           {
          // Set label
          var label = topic.toString().slice(9,13);
          //Parse data
          var temp = JSON.parse(message.toString());

          socketClient.emit('topic', { 'label': label,  'x': temp.position.x,'y': temp.position.y,'z': temp.position.z, quality: temp.position.quality, time: Date(), idMQTT: idMQTT });}       

        // Not finished
        // else  {
        //   var label =topic.toString().slice(9,13);
        //   var temp = JSON.parse(message.toString());
        //   socketClient.emit('NaN', { 'label': label,  'x': temp.position.x,'y': temp.position.y,'z': temp.position.z, quality: temp.position.quality, time: Date(), idMQTT: idMQTT });
        // }
        // */
        }
      }

      // Get device config
      if(searchconfig != -1)
      {
        var label =topic.toString().slice(9,13);
        var temp = JSON.parse(message.toString());
        if(temp.configuration.nodeType == "ANCHOR"){
          var anchor = new Object({
            label: temp.configuration.label,
            idAnchor: label,
            nodeType: temp.configuration.nodeType,
            ble: temp.configuration.ble,
            leds: temp.configuration.leds,
            uwbFirmwareUpdate: temp.configuration.uwbFirmwareUpdate,
            initiator: temp.configuration.anchor.initiator,
            x: temp.configuration.anchor.position.x,
            y: temp.configuration.anchor.position.y,
            z: temp.configuration.anchor.position.z,
            quality: temp.configuration.anchor.position.quality 
          });
          socketClient.emit('configAnchor1', { configuration: anchor, time: Date(), idMQTT: idMQTT });
        }
        else {
          if(temp.configuration.nodeType == "TAG"){
            var tag = new Object({
              label: temp.configuration.label,
              idTag: label,
              nodeType: temp.configuration.nodeType,
              ble: temp.configuration.ble,
              leds: temp.configuration.leds,
              uwbFirmwareUpdate: temp.configuration.uwbFirmwareUpdate,
              locationEngine: temp.configuration.tag.locationEngine,
              stationaryDetection: temp.configuration.tag.stationaryDetection,
              nomUpdateRate: temp.configuration.tag.nomUpdateRate,
              statUpdateRate: temp.configuration.tag.statUpdateRate,
              responsive:temp.configuration.tag.responsive
            });
            socketClient.emit('configTag1', { configuration: tag, time: Date() , idMQTT: idMQTT});
          }
        }
      }
      if(searchStatus != -1)
      {
        var label =topic.toString().slice(9,13);
        socketClient.emit('status', { label: label, status: String(JSON.parse(message.toString()).present), idMQTT: idMQTT  });
      }
      if(searchDetect != -1)
      {
        var label =topic.toString().slice(9,13);
        var temp = JSON.parse(message.toString());
        var t = base64ToHex(temp.data);
        socketClient.emit('detect', { label: label, cat: base64ToHex(temp.data) , time: Date(),battery: converter.hexToDec(t), idMQTT: idMQTT });
      }
    });
    this.mqttClient.on('close', () => {
      console.log(`mqtt client disconnected`);
    });
  }
  sendMessage(message) {
    this.mqttClient.publish('mytopic', message);
  }
   
}
function toHex(b)
{
    var h = b.toString(16);
    if (h.length == 1) {
        h = '0' + h;
    }
    return h;
}
function base64ToHex(base64String) {
  var binString = atob(base64String),
          hexArray = [];
  for (var i = 0; i < binString.length; i++) {
      hexArray.push(toHex(binString.charCodeAt(i)));
  }
  return hexArray.join('');
}

module.exports = MqttHandler;