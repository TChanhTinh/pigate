defmodule PigateWeb.TrackerLive do
  use PigateWeb, :live_view
  alias Pigate.Mqtt.Store

  @impl true
  def mount(_params, _session, socket) do
    if connected?(socket), do: Store.subscribe()
    {:ok, fetch(socket)}
  end

  @impl true
  def handle_info({:message_pushed, _message}, socket) do
    {:noreply, update(socket, :messages, Pigate.Mqtt.Store.get_messages())}
  end

  defp fetch(socket) do
    assign(socket, :messages, Pigate.Mqtt.Store.get_messages())
  end
end
