use Mix.Config

# Configure your database
#
# The MIX_TEST_PARTITION environment variable can be used
# to provide built-in test partitioning in CI environment.
# Run `mix help test` for more information.
config :pigate, Pigate.Repo,
  username: "postgres",
  password: "pigate_test!",
  database: "pigate_test",
  hostname: "postgres",
  pool: Ecto.Adapters.SQL.Sandbox

config :pigate, Pigate.Mqtt.Connection,
  host: "localhost",
  port: 1883

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :pigate, PigateWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn
