const express = require("express");
const bodyParser = require("body-parser");
const http = require("http");
const socketIO = require("socket.io");
const mqtt = require("mqtt");
const fileUpload = require("express-fileupload");
const host = "mqtt://mqtt:1883";
const apiUpdate1 = "dwm/node/";
const apiUpdate2 = "/downlink/config";
const apiUpdate3 = "/downlink/data";
const { idMQTT, hostSocket } = require("./lib/keys");
const app = express();
const btoa = require("btoa");

global.socketClient = require("socket.io-client")(hostSocket);

let allowCrossDomain = function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  next();
};
app.use(fileUpload());
app.use(allowCrossDomain);

const PORT = process.env.PORT || 3333;
//Set config file image
app.use(bodyParser.json({ limit: "50mb" }));
app.use(
  bodyParser.urlencoded({
    limit: "50mb",
    extended: true,
    parameterLimit: 50000,
  })
);

//Routes
app.use(require("./routes/index"));
app.use("/mqtt", require("./routes/mqtt"));
const server = http.createServer(app);
global.io = socketIO(server);
const mqttClient = mqtt.connect(host);

// This is what the socket.io syntax is like, we will work this later
io.on("connection", function (socket) {
  console.log("User connected", socket.id);
  socket.emit("message", { message: "hello world" });
  socket.on("disconnect", () => {
    console.log("user disconnected");
  });
  const mqttClient = mqtt.connect(host);
  socket.on("anchor", (data) => {
    const message = new Object();
    const anchor = new Object();
    const position = new Object();
    const configuration = new Object();
    message.label = data.label;
    message.nodeType = "ANCHOR";
    message.uwbFirmwareUpdate = data.firmwareUpdate;
    message.leds = data.leds;
    message.ble = data.ble;
    anchor.initiator = data.initiator;
    anchor.routingConfig = "ROUTING_CFG_OFF";
    position.x = parseFloat(data.x);
    position.y = parseFloat(data.y);
    position.z = parseFloat(data.z);
    position.quality = 100;
    anchor.position = position;
    message.anchor = anchor;
    configuration.configuration = message;
    mqttClient.publish(
      apiUpdate1 + data.id + apiUpdate2,
      JSON.stringify(configuration),
      { qos: 1 },
      (e) => {}
    );
  });
  socket.on("tag", (data) => {
    const message = new Object();
    const tag = new Object();
    const configuration = new Object();
    message.label = data.label;
    message.nodeType = "TAG";
    message.uwbFirmwareUpdate = data.uwbFirmwareUpdate;
    message.leds = data.leds;
    message.ble = data.ble;
    tag.locationEngine = data.locationEngine;
    tag.stationaryDetection = data.stationaryDetection;
    tag.nomUpdateRate = data.nomUpdateRate;
    tag.statUpdateRate = data.statUpdateRate;
    tag.responsive = data.responsive;
    message.tag = tag;
    configuration.configuration = message;
    mqttClient.publish(
      apiUpdate1 + data.id + apiUpdate2,
      JSON.stringify(configuration),
      { qos: 1 },
      (e) => {}
    );
  });
  socket.on("sendData", (data) => {
    const mes = data.message;
    const overwrite = data.overwrite;
    var dataBase64 = hexToBase64(mes);
    var message = { data: dataBase64, overwrite: overwrite };
    mqttClient.publish(
      apiUpdate1 + data.id + apiUpdate3,
      JSON.stringify(message),
      { qos: 1 },
      (e) => {}
    );
  });
});
socketClient.emit("setup", { idMQTT: idMQTT + "setup" });
socketClient.on("disconnect", () => {
  console.log(Date());
  console.log("Reconnect");
  socketClient.emit("setup", { idMQTT: idMQTT + "setup" });
});
socketClient.on("configTag", (data) => {
  const message = new Object();
  const tag = new Object();
  const configuration = new Object();
  message.label = data.configuration.label;
  message.nodeType = "TAG";
  message.uwbFirmwareUpdate = data.configuration.uwbFirmwareUpdate;
  message.leds = data.configuration.leds;
  message.ble = data.configuration.ble;
  tag.locationEngine = data.configuration.locationEngine;
  tag.stationaryDetection = data.configuration.stationaryDetection;
  tag.nomUpdateRate = data.configuration.nomUpdateRate;
  tag.statUpdateRate = data.configuration.statUpdateRate;
  tag.responsive = data.configuration.responsive;
  message.tag = tag;
  configuration.configuration = message;
  console.log(configuration);
  mqttClient.publish(
    apiUpdate1 + data.configuration.id + apiUpdate2,
    JSON.stringify(configuration),
    { qos: 1 },
    (e) => {}
  );
});
socketClient.on("configAnchor", (data) => {
  const message = new Object();
  const anchor = new Object();
  const position = new Object();
  const configuration = new Object();
  message.label = data.configuration.label;
  message.nodeType = "ANCHOR";
  message.uwbFirmwareUpdate = data.configuration.firmwareUpdate;
  message.leds = data.configuration.leds;
  message.ble = data.configuration.ble;
  anchor.initiator = data.configuration.initiator;
  anchor.routingConfig = "ROUTING_CFG_OFF";
  position.x = parseFloat(data.configuration.x);
  position.y = parseFloat(data.configuration.y);
  position.z = parseFloat(data.configuration.z);
  position.quality = 100;
  anchor.position = position;
  message.anchor = anchor;
  configuration.configuration = message;
  console.log(configuration);
  mqttClient.publish(
    apiUpdate1 + data.configuration.id + apiUpdate2,
    JSON.stringify(configuration),
    { qos: 1 },
    (e) => {}
  );
});
function hexToBase64(hexString) {
  var bytes = [],
    binString;
  for (var i = 0; i < hexString.length - 1; i += 2) {
    bytes.push(parseInt(hexString.substr(i, 2), 16));
  }
  binString = String.fromCharCode.apply(String, bytes);
  return btoa(binString);
}

server.listen(PORT, () => console.log(`Listening on ${PORT}`));
